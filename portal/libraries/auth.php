<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * Auth
	 * 
	 * @package Helpdesk
	 * @author Andri Fachrur Rozie
	 * @copyright 2010
	 * @version $Id$
	 * @access public
	 */
	class Auth {
     
	  function Auth() {
      	$this->CI =& get_instance();      	
      }
	  
      /*
      function check_auth($role) {        
        if(!$this->CI->session->userdata('logged_in')) {
            $this->logout();
        } else {
            $role = explode(",",$role);
            if (!in_array($this->role(),$role)) {
                show_404();                        
            }
        }    
      }*/
      
      
      function check_auth() {
      	if($this->CI->session->userdata('logged_in') != 1) {
      		$this->logout();      		
      	} else {		
      		$url = $this->CI->uri->segment(1);
      		$func = $this->CI->uri->segment(2);
      
      		if (! empty($func)) {
      			$url .= "/".$this->CI->uri->segment(2);
      		}
      
	  	   /*----------------------------------------------------------------------------------------------------- 
		   |  Prompt update data
		   -----------------------------------------------------------------------------------------------------*/
		    if(is_numeric ($this->CI->session->userdata('username'))&&($url != 'mahasiswa/update_data')){
				$timeperiod = get_configuration('time_period'); 
				$limit_update=substr($timeperiod,4,1)==1?(intval(substr($timeperiod,0,4))-1).'-12-01 00:00:00':intval(substr($timeperiod,0,4)).'-05-01 00:00:00';
				
				//last update
				$this->CI->db->where("nim",$this->CI->session->userdata('username'));
				$this->CI->db->select('last_update');
				$query =$this->CI->db->get('mahasiswa');
				if($query->num_rows()>0)
				{
					$row = $query->row(); 
					$last_update= $row->last_update;
				}
				$need_update=$last_update<$limit_update;
				$should_update=get_configuration('update_data_on_reregistration');
						
				if($need_update && $should_update) redirect('mahasiswa/update_data');
			}
		    //--------------------------------------------------------------------------------------------------------
	  
		   /*----------------------------------------------------------------------------------------------------- 
		   |  Prompt OSMB
		   -----------------------------------------------------------------------------------------------------*/
		   /* if(is_numeric ($this->CI->session->userdata('username'))&&($url != 'mahasiswa/osmb')&& get_configuration('prompt_osmb')){
				//last update
				$osmb_check_data=array(
					"peserta"=>$this->CI->session->userdata('username'),
					"period"=>get_configuration('time_period')
				);
				$query=$this->CI->db->get_where('osmb',$osmb_check_data);
				if($query->num_rows()>0)
				{
					
				}else{		
					redirect('mahasiswa/osmb');
				}
			}*/
		    //--------------------------------------------------------------------------------------------------------
			
      		$where = array("url" => $url);
      
      		$query = $this->CI->db->get_where('permissions',$where);
      
      		if ($query->num_rows() > 0) {
      			$row = $query->row_array();
      			
      			$permission = json_decode($row['permission']);
                
      			$intersect = array_intersect($this->CI->session->userdata('role'),$permission);
                
                if (count($intersect) > 0) {
      				return TRUE;                      
      			} else {
      				redirect('permission_error');
      			}
      		}
      
      	}
      }
      
      function login_process($login = NULL) {
	      $password = $login['password'];
          $password = hashPassword($password);  
		  
		  		  	
		  $this->CI->db->where('password',$password);
			  
	      if (is_numeric($login['username'])) {
	      	  $this->CI->db->where('nim', $login['username']);
              
	      	  $query = $this->CI->db->get('mahasiswa');
	      } else {
	      	  $this->CI->db->where('username', $login['username']);	
		      $query = $this->CI->db->get('staff');		      
	      }
		  
		  $remarks = "Username = '".$login['username']."' ";
	
	      if ($query->num_rows() >= 1) {
	      		$row = $query->row_array();
			  
			  if (is_numeric($login['username'])) {
				if($row['status']==1){
				  $newdata = array(
	                		'username'  => $login['username'],
	               			'role'  => array(9),
	               			'major'  => $row['major'],
			 		'wilayah' => $row['region'],
					'logged_in' => TRUE
	               			);  $result = "success";
				}
				else if($row['status']==0){
					$result = "failure";
					echo json_encode(array('code'=>0,'message'=>'Status kemahasiswaan anda tidak aktif. Silahkan hubungi kemahasiswaan melalui email kemahasiswaan@utkorea.org'));
	           			
				}
				else if($row['status']==2){
					$result = "failure";
	           			echo json_encode(array('code'=>0,'message'=>'Status kemahasiswaan anda cuti. Silahkan hubungi kemahasiswaan melalui email kemahasiswaan@utkorea.org'));	
				}
				else if($row['status']==3){
					$result = "failure";
	           			echo json_encode(array('code'=>0,'message'=>'Status kemahasiswaan anda alumni. Silahkan hubungi kemahasiswaan melalui email kemahasiswaan@utkorea.org'));	
				}
	                           
			  } else {
			        $role = explode(',',$row['group_id']);
			  	$newdata = array(
	                   		'username'  => $login['username'],
	                   		'id'  => $row['staff_id'],
	                   		'role'  => $role,
	                   		'major'  => $row['major_id'],
	                   		'logged_in' => TRUE
	               		); 
				$result = "success";
			  }	
               
               
              
               		      			      	
	      } else {
	      	  $result = "failure";
	          echo json_encode(array('code'=>0,'message'=>'Username atau Password Tidak Sesuai. Jika kesulitan login silahkan gunakan fitur lupa password.'));
		  }
			
		if($result == "success"){
			$this->CI->session->set_userdata($newdata);
			echo json_encode(array('code'=>1,'message'=>'Login sukses'));
		}
		  
		  $logs_data = array('activity' => 'login',
		  					 'result' => $result,
							 'remarks' => $remarks,
							 'user_agent' => check_user_agent());
							 
		 insert_logs($logs_data);	  
    }
      
	function login_as($login = NULL) {
		if($login==NULL)return 0;
		
	      if (is_numeric($login['username'])) {
	      	  $this->CI->db->where('nim', $login['username']);
              $this->CI->db->where('status', '1');
	      	  $query = $this->CI->db->get('mahasiswa');
	      } else {
	      	  $this->CI->db->where('username', $login['username']);	
		      $query = $this->CI->db->get('staff');		      
	      }
		  
		  $remarks = "Superuser login as Username = '".$login['username']."' ";
	
	      if ($query->num_rows() >= 1) {
	      		$row = $query->row_array();
			  
			  if (is_numeric($login['username'])) {
	                $newdata = array(
	                   'username'  => $login['username'],
	                   'role'  => array(9),
	                   'major'  => $row['major'],
			   'wilayah' => $row['region'],
	                   'logged_in' => TRUE
	               );              
			  } else {
			        $role = explode(',',$row['group_id']);
			  		$newdata = array(
	                   'username'  => $login['username'],
	                   'id'  => $row['staff_id'],
	                   'role'  => $role,
	                   'major'  => $row['major_id'],
	                   'logged_in' => TRUE
	               );
			  }	
               
               $this->CI->session->set_userdata($newdata);
               $result = "success";		      	
	      } else {
	      	  $result = "failure";
		  }
		  
		  $logs_data = array('activity' => 'login',
		  					 'result' => $result,
							 'remarks' => $remarks,
							 'user_agent' => check_user_agent());
							 
		 insert_logs($logs_data);	  
    }
	
	function logout() {
      	$this->CI->session->sess_destroy();
		redirect('');
      	return TRUE;
    }
}
?>