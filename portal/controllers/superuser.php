<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class superuser extends CI_Controller {

	public function __construct()
    {
        parent::__construct();								
    	//$this->output->enable_profiler(TRUE);
		$this->load->helper('superuser_helper');
		$this->load->model('utility_model','utility');
		 $this->load->model('person_model','person');
				
	    $this->load->library('email');
		$this->email->set_newline("\r\n");		
				
		$this->email->from($this->config->item('mail_from'), $this->config->item('mail_from_name'));  
    }	
	
	public function index()
	{
	    $this->auth->check_auth();	   
	}
	
	public function manajemen_menu(){
		$this->auth->check_auth();	   
		$data['list_menu'] = menu_editor_menu_list();	
		$content['page'] = $this->load->view('superuser/manajemen_menu',$data,TRUE);
        $this->load->view('dashboard',$content);	
	}
	
	function update_menu_status(){
		$result=$this->utility->update_menu_status(str_replace("menu-","",$this->input->post('name')),$this->input->post('status')=='true'?1:0);
	}
	
	function check_and_fix_student_class(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nim', 'NIM', 'required|numeric');
		$this->form_validation->set_rules('periode', 'Periode', 'required|numeric');
		$data['message_type'] = 0;
		$data['message'] = '';
		if($this->form_validation->run()){
			$student = $this->utility->get_Table('mahasiswa',array('nim'=>$this->input->post('nim')));
			if($student->num_rows()>0){
				$unins = array();
				$student = $student->result_array();				
				$res = $this->utility->check_valid_class($this->input->post('nim'),$this->input->post('periode'),calculate_semester($student[0]['entry_period']),$student[0]['region']);
				if($res->num_rows()>0){
					$res = $res->result_array();					
					$res2 = $this->utility->get_Table('class',array('id_student'=>$this->input->post('nim')),'id_assignment');
					if($res2->num_rows()>0){						
						$res2 = $res2->result_array();
						
						foreach($res as $row){
							if(!in_array($row['id'],$res2[0])){
								$unins[] = array('id_student'=>$this->input->post('nim'),'id_assignment'=>$row['id']);
							}
						}						
						if(count($unins)>2){
							//Deleting All class for this student
							$this->utility->remove_from_Table('class',array('id_student'=>$this->input->post('nim')));
						}
						if(count($unins)>0){
							$this->utility->insert_batch_Table('class',$unins);	
							$data['message'] .= 'Berhasil Memperbaiki '.count($unins).' Kelas untuk Student NIM = '.$this->input->post('nim');								
						}else{
							$data['message'] .= 'Kelas Sudah sesuai untuk Student NIM = '.$this->input->post('nim');							
						}
					}else{
						$data['message'] .= 'Berhasil Menambahkan <b>3</b> Kelas untuk Student NIM = '.$this->input->post('nim');						
						foreach($res as $row){
							$unins[] = array('id_student'=>$this->input->post('nim'),'id_assignment'=>$row['id']);	
						}
						$this->utility->insert_batch_Table('class',$unins);							
					}					
				}
				
			}			
		}		
		$data = array();
		$content['page'] = $this->load->view('superuser/checkfixclass',$data,TRUE);
        $this->load->view('dashboard',$content);
	}
	
	public function login_as(){
		$this->auth->check_auth();
		$this->auth->login_as($_GET);
		redirect('');			
	}
	
	function reset_pwd($user){
		$this->auth->check_auth();
		if (is_numeric($user)) {
	      	  $table='mahasiswa';
	      } else {
	      	  $table='staff';		      
	      }
		$this->person->change_password($table,$user,$user);
		redirect('');			
	}
	
	function staff_management() {
		$this->auth->check_auth();
		$data = array();

		$bucket = $this->utility_model->get_Table('usergroups');
		 $usergroups = array();
		 foreach($bucket->result_array() as $row){
			$usergroups[] = $row;
		 }

		 $region_arr = array();
		 $region_arr['K'] = 'Seoul';
		 $region_arr['A'] = 'Ansan';
		 $region_arr['G'] = 'Guro';
		 $region_arr['U'] = 'Ujiongbu';
		 $region_arr['D'] = 'Daegu';
		 $region_arr['C'] = 'Cheonan';
		 $region_arr['J'] = 'Gwangju';
		 $region_arr['B'] = 'Busan';
         $data = array('usergroups'=>$usergroups,
                       'region_arr'=>$region_arr);
		$content['page'] = $this->load->view('superuser/staff_management',$data,TRUE);
        $this->load->view('dashboard',$content);		
	}
	
	public function staffCRUD(){
		$response = "";
		if(isset($_POST['oper'])){
			$col = array();
			$this->load->library('form_validation');
			
			switch($_POST['oper']){				
				case 'edit':
					$this->form_validation->set_rules('id', 'Staff ID', 'required|numeric');
					$this->form_validation->set_rules('name', 'Name', 'required');
					$this->form_validation->set_rules('phone', 'Phone', 'required|numeric|trim|xss_clean');					
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');				
					$col = $this->input->post();
					break;
				case 'add':
					$this->form_validation->set_rules('name', 'Name', 'required');
					$this->form_validation->set_rules('phone', 'Phone', 'required|numeric|trim|xss_clean');					
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');	
					$col = $this->input->post();
					break;
				case 'del':
					$this->form_validation->set_rules('id', 'Staff ID', 'required|numeric');	
					$col = $this->input->post();
					break;
				default:
					exit;
			}
			if ($this->form_validation->run())
			{
					switch($col['oper']){
						case 'edit':
							//print_r($col);
							unset($col['oper']);
							$id = $col['staff_id'];
							unset($col['id']);
							$this->person->update_tutor($id,$col);
							$response ='Update success!';
							break;
						case 'add':
							unset($col['oper']);
							unset($col['id']);				
							if($this->person->check_username($col['username'])){
								$response ='Username already exist!';
							}else{
								$this->person->add_staff($col);
								$response ='Add user success!';
							}
							break;
						case 'del':
							$this->person->delete_tutor($col['id']);
							break;
						default:
							exit;
					}
			}else{
					$response = validation_errors();
			}
		}				
		
		echo $response;		
	}
	
	//buat kirim password ke tutor baru, ganti baris di template email tidak berfungsi	
	public function mail_tutor_ranged($start=0,$end=0){
		
		//$list = $this->person->get_list_mahasiswa();
		$this->load->model('tutor_model');
		$list = $this->tutor_model->get_tutor_ranged($start);
		
		$i = 0;
		
		$from_name = 'IT UTKOREA';
        $from_email = 'it@utkorea.org';
	    $subject = "Account Portal Akademik Universitas Terbuka Korea Selatan";

		        
		
		foreach ($list->result() as $row) {
			$name = $row->name;
			$username = $row->username;
			$password = $row->username;
			$email = $row->email;
			echo $email.'<br/>';
			$message = $this->lang->line('mail_portal_info_tutor');			
			$message = sprintf($message,$name,$username,$password);    
			native_mail($email,$from_name,$from_email,$subject,$message);
					
			$i++;	
		}			
	}
	
}