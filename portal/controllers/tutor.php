<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tutor extends CI_Controller {

	public function __construct()
    {
        parent::__construct();								
		//$this->output->enable_profiler(TRUE);
        $this->load->model('person_model','person');
		$this->load->model('tutor_model');
		$this->load->model('class_model');
		$this->load->model('person_model');
    }	
	
	public function index()
	{
		$this->auth->check_auth();
		
        $bucket = $this->utility_model->get_Table('major');
		 
		$major_arr = array();
		foreach($bucket->result_array() as $row){
		  $major_arr[] = $row;
		}
		
        $region_arr = array();
		$region_arr['Utara'] = 'Utara';
		$region_arr['Selatan'] = 'Selatan';
		
		$content['page'] = $this->load->view('tutor/tutor',array('major_arr'=>$major_arr,'region_arr'=>$region_arr),TRUE);		 
        $this->load->view('dashboard',$content);       
	}
	
	public function exportCurrentCRUD(){
		$page = $this->input->get("page", TRUE );
		if(!$page)$page=1;
		
		$rows = $this->input->get("rows", TRUE );
		if(!$rows)$rows=10;
		
		$sort_by = $this->input->get( "sidx", TRUE );
		if(!$sort_by)$sort_by='name';
		
		$sort_direction = $this->input->get( "sord", TRUE );
		if(!$sort_direction)$sort_direction='ASC';
		
		$req_param = array (
            "sort_by" => $sort_by,
			"sort_direction" => $sort_direction,
			"page" => $page,
			"rows" => $rows,
			"search" => $this->input->get( "_search", TRUE ),
			"search_field" => $this->input->get( "searchField", TRUE ),
			"search_operator" => $this->input->get( "searchOper", TRUE ),
			"search_str" => $this->input->get( "searchString", TRUE )
		);
		
		$this->jqgrid_export->exportCurrent('tutor',$req_param);
	}
	
	public function CRUD(){
		$response = "";
		if(isset($_POST['oper'])){
			$col = array();
			$this->load->library('form_validation');
			
			switch($_POST['oper']){				
				case 'edit':
					$this->form_validation->set_rules('id', 'Staff ID', 'required|numeric');
					$this->form_validation->set_rules('name', 'Name', 'required');
					$this->form_validation->set_rules('major_id', 'Major', 'required|numeric');
					$this->form_validation->set_rules('region', 'Region', 'required');
					$this->form_validation->set_rules('phone', 'Phone', 'required|numeric|trim|xss_clean');					
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					$this->form_validation->set_rules('birth', 'Birth', 'xss_clean');
					$this->form_validation->set_rules('affiliation', 'affiliation', 'xss_clean');					
					$col = $this->input->post();
					break;
				case 'add':
					$this->form_validation->set_rules('name', 'Name', 'required');
					$this->form_validation->set_rules('major_id', 'Major', 'required|numeric');
					$this->form_validation->set_rules('region', 'Region', 'required');
					$this->form_validation->set_rules('phone', 'Phone', 'required|numeric|trim|xss_clean');					
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					$this->form_validation->set_rules('birth', 'Birth', 'xss_clean');
					$this->form_validation->set_rules('affiliation', 'affiliation', 'xss_clean');		
					$col = $this->input->post();
					break;
				case 'del':
					$this->form_validation->set_rules('id', 'Staff ID', 'required|numeric');	
					$col = $this->input->post();
					break;
				default:
					exit;
			}
			if ($this->form_validation->run())
			{
					switch($col['oper']){
						case 'edit':
							unset($col['oper']);
							$id = $col['id'];
							unset($col['id']);
							$this->person->update_tutor($id,$col);
							break;
						case 'add':
							unset($col['oper']);
							unset($col['id']);							
							$this->person->add_tutor($col);
							break;
						case 'del':
							$this->person->delete_tutor($col['id']);
							break;
						default:
							exit;
					}
			}else{
					$response = validation_errors();
			}
		}				
		
		echo $response;		
	}	

	public function assignment() {
		$this->auth->check_auth();
		$data = array();
		
		$major = array(0 => "-- Pilih Jurusan --");
		$data['major'] = $major + major_list();		
		
		$region = array(0 => "-- Pilih Lokasi --","Utara","Selatan");
		$data['region'] = $region;
		
		$content['page'] = $this->load->view('tutor/assignment',$data,TRUE);
        $this->load->view('dashboard',$content);		
	}
	
	public function assignment_major($id,$region) {
		//$this->auth->check_auth();
		$data = array();
		$i = 1;
			
		$courses = array();
		
		foreach ($this->tutor_model->course_list($id,$region) as $row) {
			if ($row->semester <> $i) {
				$i = $row->semester;				
			}
			
			$courses[$i][] = $row;
		}				
		$tutor = array(0 => "-- Pilih Tutor --");
		$data['tutor'] = $tutor + $this->tutor_model->tutor_by_major($id);
		$data['course'] = $courses;
		$data['region'] = $region;
		$this->load->view('tutor/assignment_major',$data);
	}
	
	public function save_assignment() {
		$region = $this->input->post('region');
		
		$i = 0;
		$data = explode('&',$this->input->post('frmdata'));
		foreach ($data as $key => $val) {
			$val = explode('=',$val);
			if (! empty($val[1])) {
				$course_id = str_replace('tutor', '', $val[0]);
				
				$insert = $this->tutor_model->save_assignment($val[1], $course_id,$region);
				
				if ($insert) {
					$i++;	
				}
			}	
		}
		
		echo ($i > 0)? '1' : '0';
	}
    
    public function transport(){
		 $this->auth->check_auth();
		 $timeperiod = get_configuration('time_period');

		 $this->load->library('form_validation');
		 $this->form_validation->set_rules('tanggalttm', 'Tanggal TTM', 'required');
		 $this->form_validation->set_rules('total', 'Total Pengeluaran', 'required|numeric');
		 $this->form_validation->set_rules('deskripsi', 'Deskripsi Perjalanan', 'required');
		 $this->form_validation->set_rules('masukan', 'Masukan TTM', 'xss_clean');
		 $this->form_validation->set_rules('waktudibayar', 'Waktu Dibayar', 'required');
		 
		 $this->load->model('finance_model');
		 $data['success'] = 0;
		 
		 if($this->form_validation->run()){
		 	$data = array();
		 	$data = $this->input->post();
			unset($data['simprev']);
			$data['staff_id'] = $this->session->userdata('id');
			$data['period'] = $timeperiod;
			$this->finance_model->save_transport($data);
			
			$data['success'] = 1;
			$data['error'] = '';
		 }else{
			$data['error'] = validation_errors();
		}
					
		 
		 $data['transport'] = $this->finance_model->get_my_transport($this->session->userdata('id'),$timeperiod);
		  
		 $content['page'] = $this->load->view('tutor/transport',$data,TRUE);
		 $this->load->view('dashboard',$content);
	}
	
	function get_tutor($id,$output="json"){
		$res = $this->tutor_model->get_tutor($id);
		if($res){
			switch ($output){
				case "json":
						echo json_encode($res);
						break;
				case "html":												
						$this->load->view('tutor/person_table_view',array('data'=>$res));
						break;
			}
			
		}
	}

	function gabung_kelas(){
		$this->auth->check_auth();	
		$this->load->model('class_model');
		
		$data['asgnmt_list'] = $this->tutor_model->get_assignment(setting_val('time_period'));
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('from_assignment', 'From Assignment', 'required');
		$this->form_validation->set_rules('to_assignment', 'To Assignment', 'required');
		
		if($this->form_validation->run()){
				$col = $this->input->post();
				
				$this->class_model->add_gabung_kelas($col);
								
		}
		
		
		$data['list'] = $this->class_model->get_list_gabung_kelas();
		
		$content['page'] = $this->load->view('tutor/gabung_kelas',$data,TRUE);
		$this->load->view('dashboard',$content);
	}
	
	function verify_transport(){
		$this->form_validation->set_rules('id','id','trim|required');
		if($this->form_validation->run())
		{
			$this->load->model('finance_model','finance');		
			$this->finance->update_transport($this->input->post('id'),array('is_verified'=>2,'verified_by'=>$this->session->userdata('id'),'verified_time'=>date("Y-m-d H:i:s")));
			echo "";
		}else{
			echo validation_errors();
		}
	}
	
	function export_nilai($period=null){
		if(empty($period))$period=get_configuration('time_period');		$q = $this->tutor_model->get_current_class_composition_list($period);		$currYear=Date('Y');		for($y=2013,$i=0;$y<=$currYear;$y++,$i+=2){			$period_list[$i]=$y.'1';			if(get_configuration('time_period')==$period_list[$i])break;			$period_list[$i+1]=$y.'2';		}		$data['curr_period']=$period;		$data['period_list']=$period_list;
		
		$data['list'] = $q;
		$content['page'] = $this->load->view('tutor/export_nilai',$data,TRUE);
		$this->load->view('dashboard',$content);
	}
	

public function exp($sid) 
	{
		
		$class = $this->tutor_model->get_class_by_id($sid);
		
		$this->load->model('class_model','cls');
		$students1 = $this->cls->list_class_student($sid)->result();
                $students2 = $this->cls->list_class_student($sid)->result();
$m=array_merge($students1,$students2);
print_r($m);
}
	public function export_to_excel($sid) 
	{
		
		$class = $this->tutor_model->get_class_by_id($sid);
		
		

		$this->load->model('class_model','cls');
		$studentslist = array();
		if(!is_null($this->cls->list_class_student($sid))){
			$studentslist = $this->cls->list_class_student($sid)->result();
		}
		
		if($class->from_assignment!=NULL){
			if(!is_null($this->cls->list_class_student($class->to_assignment))){
				$gabunglist =  $this->cls->list_class_student($class->to_assignment)->result();
				$studentslist = array_merge($studentslist,$gabunglist);	
			}
		}

		
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		
		$current_period = get_settings('time_period');
		
		$style_title = array(					
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'font' => array(
						'bold' => true,
					)
				);
				
		$style_table_header = array(					
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
						),
					)
				);
				
				
		$style_table = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
						),
					)
				);
				
		$this->excel->getDefaultStyle()->getFont()->setName('Verdana');
		$this->excel->getDefaultStyle()->getFont()->setSize(10);
		
		// Add an image to the worksheet
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('UT LOGO');		
		$objDrawing->setPath('assets/core/images/logo_ut.jpg');
		$objDrawing->setWidth(40);
		$objDrawing->setHeight(40);
		$objDrawing->setCoordinates('B1');
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		
		$this->excel->getActiveSheet()->setTitle('Nilai');
                $this->excel->getActiveSheet()->getStyle('H1')->applyFromArray($style_table_header);
                $this->excel->getActiveSheet()->setCellValue('H1', 'UM00-RK05-R2');
$this->excel->getActiveSheet()->getStyle("H1")->getFont()->setBold(true)->setName('Verdana')->setSize(10)->getColor()->setRGB('000000');
		$this->excel->getActiveSheet()->setCellValue('D2', 'Rekap Nilai Tugas Tutorial Tatap Muka');
		$this->excel->getActiveSheet()->setCellValue('D3', 'KOREA Masa Registrasi '.substr($current_period, 0,4).'.'.substr($current_period, 4,1));
               $this->excel->getActiveSheet()->getStyle("D2")->getFont()->setBold(true)->setName('Verdana')->setSize(12)->getColor()->setRGB('000000');
$this->excel->getActiveSheet()->getStyle("D3")->getFont()->setBold(true)->setName('Verdana')->setSize(12)->getColor()->setRGB('000000');

		$this->excel->getActiveSheet()->mergeCells('D3:H3');
                $this->excel->getActiveSheet()->mergeCells('D2:H2');
                $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($style_title);
                $this->excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_title);
		
		$this->excel->getActiveSheet()->setCellValue('A4', 'UPBJJ-UT:');
		$this->excel->getActiveSheet()->mergeCells('A4:B4');	
		$this->excel->getActiveSheet()->setCellValue('A5', 'PROGRAM STUDI:');
		$this->excel->getActiveSheet()->mergeCells('A5:B5');
		$this->excel->getActiveSheet()->setCellValue('A6', 'KODE MATAKULIAH:');
		$this->excel->getActiveSheet()->mergeCells('A6:B6');
		$this->excel->getActiveSheet()->setCellValue('A7', 'NAMA MATAKULIAH:');
		$this->excel->getActiveSheet()->mergeCells('A7:B7');
		
		$this->excel->getActiveSheet()->setCellValue('C4', 'LAYANAN LUAR NEGERI');
 
                switch($class->major){
                case "Sastra Inggris Bidang Minat Penerjemahan" : $major="Sastra Inggris Bidang Minat Penerjemahan / 87";break;
                case "Manajemen-S1" : $major="Manajemen-S1 / 54";break;
                case "Ilmu Komunikasi-S1" : $major="Ilmu Komunikasi-S1 / 72";break;
                }
		$this->excel->getActiveSheet()->setCellValue('C5', $major);
		$this->excel->getActiveSheet()->setCellValue('C6', $class->code);
		$this->excel->getActiveSheet()->setCellValue('C7', $class->title);
		
		$this->excel->getActiveSheet()->setCellValue('H4', 'TEMPAT UJIAN:');
		$this->excel->getActiveSheet()->setCellValue('H5', 'SEMESTER:');
		$this->excel->getActiveSheet()->setCellValue('H6', 'NAMA TUTOR:');
		$this->excel->getActiveSheet()->setCellValue('H7', 'KELAS:');
		
		if($class->from_assignment!=NULL){
		$ExamRegion='Ansan dan Daegu, Korea Selatan* /969 & 971'; $classRegion="Ansan dan Daegu";
		}else{
			if($class->region=='Selatan'){$ExamRegion='Daegu, Korea Selatan* /971'; $classRegion="Daegu";
			}else{$ExamRegion='Ansan, Korea Selatan* /969';$classRegion="Ansan";}
		}
		$this->excel->getActiveSheet()->setCellValue('I4', $ExamRegion);
		$this->excel->getActiveSheet()->setCellValue('I5', $class->semester);
		$this->excel->getActiveSheet()->setCellValue('I6', $class->name);
if($classRegion=="Daegu"){$class_code="B";}elseif($classRegion=="Ansan"){$class_code="A";}else{$class_code="A dan B";}
		$this->excel->getActiveSheet()->setCellValue('I7',$class_code );
		
		//Header Table
		$this->excel->getActiveSheet()->setCellValue('A9', 'No');
		$this->excel->getActiveSheet()->getStyle('A9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('B9', 'NIM');
		$this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('C9', 'Nama Mahasiswa');
		$this->excel->getActiveSheet()->getStyle('C9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('D9', 'Tugas(TA)');
		$this->excel->getActiveSheet()->getStyle('D9:G9')->applyFromArray($style_table_header);	
		$this->excel->getActiveSheet()->mergeCells('D9:G9');			
		$this->excel->getActiveSheet()->setCellValue('H9', 'Nilai Partisipasi(P)');
		$this->excel->getActiveSheet()->getStyle('H9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('I9', 'Nilai Akhir');
		$this->excel->getActiveSheet()->getStyle('I9')->applyFromArray($style_table_header);
		
		$this->excel->getActiveSheet()->getStyle('A10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('B10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('C10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('D10', '1');
		$this->excel->getActiveSheet()->getStyle('D10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('E10', '2');
		$this->excel->getActiveSheet()->getStyle('E10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('F10', '3');
		$this->excel->getActiveSheet()->getStyle('F10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('G10', 'NTT');
		$this->excel->getActiveSheet()->getStyle('G10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('H10')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('I10')->applyFromArray($style_table_header);
						
		$c=11;$i=1;	
					
		foreach($studentslist as $row){
			$this->excel->getActiveSheet()->setCellValue('A'.$c, $i);
			$this->excel->getActiveSheet()->getStyle('A'.$c)->applyFromArray($style_table);
			
			//NIM
			$this->excel->getActiveSheet()->setCellValue('B'.$c, $row->nim);
			$this->excel->getActiveSheet()->getStyle('B'.$c)->applyFromArray($style_table_header);
			
			//Nama Mahasiswa
			$this->excel->getActiveSheet()->setCellValue('C'.$c, $row->name);
			$this->excel->getActiveSheet()->getStyle('C'.$c)->applyFromArray($style_table);
			
			//TUGAS 1
			$this->excel->getActiveSheet()->setCellValue('D'.$c, $row->tugas1);
			$this->excel->getActiveSheet()->getStyle('D'.$c)->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode('0.00'); 
			
			//TUGAS 2
			$this->excel->getActiveSheet()->setCellValue('E'.$c, $row->tugas2);
			$this->excel->getActiveSheet()->getStyle('E'.$c)->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('E'.$c)->getNumberFormat()->setFormatCode('0.00'); 
			
			//TUGAS 3
			$this->excel->getActiveSheet()->setCellValue('F'.$c, $row->tugas3);
			$this->excel->getActiveSheet()->getStyle('F'.$c)->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('F'.$c)->getNumberFormat()->setFormatCode('0.00'); 
			
			//AVG TUGAS
			$this->excel->getActiveSheet()->setCellValue('G'.$c, '=AVERAGE(D'.$c.':F'.$c.')');
			$this->excel->getActiveSheet()->getStyle('G'.$c)->applyFromArray($style_table);
			$this->excel->getActiveSheet()->getStyle('G'.$c)->getNumberFormat()->setFormatCode('0.00'); 
			
			//NILAI PARTISIPASI
			$this->excel->getActiveSheet()->setCellValue('H'.$c, $row->partisipasi);
			$this->excel->getActiveSheet()->getStyle('H'.$c)->applyFromArray($style_table);
			$this->excel->getActiveSheet()->getStyle('H'.$c)->getNumberFormat()->setFormatCode('0.00'); 
			
			//NILAI AKHIR
			$this->excel->getActiveSheet()->setCellValue('I'.$c, '=0.7*G'.$c.'+0.3*H'.$c);
			$this->excel->getActiveSheet()->getStyle('I'.$c)->applyFromArray($style_table);
			$this->excel->getActiveSheet()->getStyle('I'.$c)->getNumberFormat()->setFormatCode('0.00'); 
			
			$i++;
			$c++;
		}
		
		
		$this->excel->getActiveSheet()->setCellValue('G'.($c+2), $classRegion.', '.date('j F Y'));
		$this->excel->getActiveSheet()->setCellValue('G'.($c+3), 'Tutor,');
		
		
		$this->excel->getActiveSheet()->setCellValue('G'.($c+8), $class->name);
		
		
		//====================Absensi===========================================================
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex(1);
		$this->excel->getActiveSheet()->setTitle('Absensi');
		
		// Add an image to the worksheet
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('UT LOGO');		
		$objDrawing->setPath('assets/core/images/logo_ut.jpg');
		$objDrawing->setWidth(40);
		$objDrawing->setHeight(40);
		$objDrawing->setCoordinates('C1');
		$objDrawing->setWorksheet($this->excel->getActiveSheet());
		
		$this->excel->getActiveSheet()->setCellValue('D2', 'DAFTAR HADIR MATA KULIAH');		
		$this->excel->getActiveSheet()->mergeCells('D2:L2');
		$this->excel->getActiveSheet()->getStyle('D2:L2')->applyFromArray($style_title);	
		
		$this->excel->getActiveSheet()->setCellValue('C4', 'Negara:');$this->excel->getActiveSheet()->setCellValue('D4', 'Korea Selatan');
		$this->excel->getActiveSheet()->setCellValue('H4', 'Semester:');$this->excel->getActiveSheet()->setCellValue('I4', $class->semester);
		
		$this->excel->getActiveSheet()->setCellValue('C5', 'Jurusan:');$this->excel->getActiveSheet()->setCellValue('D5', $class->major);
		$this->excel->getActiveSheet()->setCellValue('H5', 'Nama Tutor:');$this->excel->getActiveSheet()->setCellValue('I5', $class->name);
		
		$this->excel->getActiveSheet()->setCellValue('C6', 'Kode/Nama Mtk:');$this->excel->getActiveSheet()->setCellValue('D6', $class->code);
		$this->excel->getActiveSheet()->setCellValue('H6', 'Kelas:');$this->excel->getActiveSheet()->setCellValue('I6', $class->title);
		
		$this->excel->getActiveSheet()->setCellValue('B8', 'No.');
		$this->excel->getActiveSheet()->mergeCells('B8:B9');
		$this->excel->getActiveSheet()->getStyle('B8:B9')->applyFromArray($style_table_header);	
		
		$this->excel->getActiveSheet()->setCellValue('C8', 'NIM');
		$this->excel->getActiveSheet()->mergeCells('C8:C9');
		$this->excel->getActiveSheet()->getStyle('C8:C9')->applyFromArray($style_table_header);	
		
		$this->excel->getActiveSheet()->setCellValue('D8', 'Nama Mahasiswa');
		$this->excel->getActiveSheet()->mergeCells('D8:D9');
		$this->excel->getActiveSheet()->getStyle('D8:D9')->applyFromArray($style_table_header);
		
		$this->excel->getActiveSheet()->setCellValue('E8', 'Pertemuan Ke-');
		$this->excel->getActiveSheet()->mergeCells('E8:L8');
		$this->excel->getActiveSheet()->getStyle('E8:L8')->applyFromArray($style_table_header);
		
		$this->excel->getActiveSheet()->setCellValue('E9', 'I');
		$this->excel->getActiveSheet()->getStyle('E9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('F9', 'II');
		$this->excel->getActiveSheet()->getStyle('F9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('G9', 'III');
		$this->excel->getActiveSheet()->getStyle('G9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('H9', 'IV');
		$this->excel->getActiveSheet()->getStyle('H9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('I9', 'V');
		$this->excel->getActiveSheet()->getStyle('I9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('J9', 'VI');
		$this->excel->getActiveSheet()->getStyle('J9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('K9', 'VII');
		$this->excel->getActiveSheet()->getStyle('K9')->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->setCellValue('L9', 'VIII');
		$this->excel->getActiveSheet()->getStyle('L9')->applyFromArray($style_table_header);
		
		
		
		//content absen
		$i = 1;
		$startrow = 9;
		foreach($studentslist as $row){
			$this->excel->getActiveSheet()->setCellValue('B'.($startrow+$i), $i);
			$this->excel->getActiveSheet()->getStyle('B'.($startrow+$i))->applyFromArray($style_table_header);
			
			$this->excel->getActiveSheet()->setCellValue('C'.($startrow+$i), $row->nim);
			$this->excel->getActiveSheet()->getStyle('C'.($startrow+$i))->applyFromArray($style_table_header);	
			
			$this->excel->getActiveSheet()->setCellValue('D'.($startrow+$i), $row->name);
			$this->excel->getActiveSheet()->getStyle('D'.($startrow+$i))->applyFromArray($style_table_header);		
			
			$temparr = explode(',',$row->absensi);
			if(!empty($temparr)){
				foreach($temparr as $abs){
					switch($abs){
						case 1:
							$this->excel->getActiveSheet()->setCellValue('E'.($startrow+$i), 'v');
							break;
						
						case 2:
							$this->excel->getActiveSheet()->setCellValue('F'.($startrow+$i), 'v');
							break;
							
						case 3:
							$this->excel->getActiveSheet()->setCellValue('G'.($startrow+$i), 'v');
							break;
							
						case 4:
							$this->excel->getActiveSheet()->setCellValue('H'.($startrow+$i), 'v');
							break;
						
						case 5:
							$this->excel->getActiveSheet()->setCellValue('I'.($startrow+$i), 'v');
							break;
						
						case 6:
							$this->excel->getActiveSheet()->setCellValue('J'.($startrow+$i), 'v');
							break;
						
						case 7:
							$this->excel->getActiveSheet()->setCellValue('K'.($startrow+$i), 'v');
							break;
						
						case 8:
							$this->excel->getActiveSheet()->setCellValue('L'.($startrow+$i), 'v');
							break;					
					}			
						
				}
			}
			
			$this->excel->getActiveSheet()->getStyle('E'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('F'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('G'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('H'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('I'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('J'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('K'.($startrow+$i))->applyFromArray($style_table_header);
			$this->excel->getActiveSheet()->getStyle('L'.($startrow+$i))->applyFromArray($style_table_header);
			
			$i++;
		}
		//==end content
		
		$this->excel->getActiveSheet()->setCellValue('D'.($startrow+$i+2), 'Nama Tutor');$this->excel->getActiveSheet()->setCellValue('D'.($startrow+$i+3), $class->name);
		$this->excel->getActiveSheet()->setCellValue('E'.($startrow+$i+2), 'Paraf Tutor');
		$this->excel->getActiveSheet()->mergeCells('E'.($startrow+$i+2).':L'.($startrow+$i+2));
		
		$this->excel->getActiveSheet()->getStyle('D'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('E'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('F'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('G'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('H'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('I'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('J'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('K'.($startrow+$i+3))->applyFromArray($style_table_header);
		$this->excel->getActiveSheet()->getStyle('L'.($startrow+$i+3))->applyFromArray($style_table_header);
		
		
		$this->excel->getActiveSheet()->setCellValue('I'.($startrow+$i+6), 'Tutor');
		$this->excel->getActiveSheet()->setCellValue('I'.($startrow+$i+7), $class->name);
		
				 
		$filename=$class->title.'_'.$class->name.'_'.$classRegion.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		     
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');		
		$objWriter->save('php://output');
		 
	}

	public function hasil_survey($period=null){		if(empty($period))$period=get_configuration('time_period');
		$q = $this->tutor_model->get_current_class_composition_list($period);		$currYear=Date('Y');		for($y=2013,$i=0;$y<=$currYear;$y++,$i+=2){			$period_list[$i]=$y.'1';			if(get_configuration('time_period')==$period_list[$i])break;			$period_list[$i+1]=$y.'2';		}		$data['curr_period']=$period;		$data['period_list']=$period_list;

		$data['list'] = $q;
                $period['period']=$period;
		$content['page'] = $this->load->view('tutor/hasil_survey',$data,TRUE);
		$this->load->view('dashboard',$content);
	}
	
	public function export_hasil_survey_all($period=20131){
		$this->load->model('class_model','cls');
		$q1 = $this->tutor_model->get_current_class_composition_list($period);
		
		$finalarray = array();
		
		foreach($q1->result() as $rowq){
			
			$q2 = $this->cls->get_survey_result($rowq->sid);
			if($q2){
				$data['surveyques'] = array(
					array('Bagian Pendahuluan','A'),
					array('Keterampilan menarik perhatian mahasiswa (ice breaking, kuis, games)',true),
					array('Keterampilan memotivasi mahasiswa',true),	
					array('Keterampilan menjelaskan ruang lingkup materi yang akan dibahas',true),
					array('Keterampilan menyampaikan relevansidanmanfaatmateri yang dibahas',true),
					array('Keterampilan menjelaskan kompetensi mahasiswa dalam mengikuti tutorial ',true),
					array('Bagian Penyajian ','B'),
					array('Sistematika penyampaian materi tutorial',true),
					array('Keterampilan menjelaskan materi tutorial',true),
					array('Keterampilan membimbing mahasiswa berlatih',true),
					array('Keterampilan mengajukan pertanyaan',true),
					array('Keterampilan memberikan umpan balik terhadap pertanyaan mahasiswa',true),
					array('Keterampilan menyebarkan pertanyaan kepada mahasiswa',true),
					array('Keterampilan mengaktifkan mahasiswa dalam tutorial',true),
					array('Keterampilan menggunakan media dan  alat pembelajaran',true),
					array('Keterampilan menggunakan beragam metode/model tutorial',true),
					array('Bagian Penutup','C'),
					array('Keterampilan menyimpulkan materi tutorial',true),
					array('Keterampilan mengevaluasi hasil belajar mahasiswa dalam tutorial',true),
					array('Keterampilan memberikan umpan balik terhadap hasil belajar mahasiswa dalam tutorial',true),
					array('Keterampilam memberikan informasi tentang kegiatan tindak lanjut',true),
					array('Keterampilan mengelola waktu tutorial ',true),
					array('Keseluruhan',true),
				);
				
				$total = array();
				//initializing
				foreach($data['surveyques'] as $key=>$p){
					$total[$key] = 0;
				}
				
				$comment = array();
				
	
				$numresponden = $q2->num_rows();
				
				foreach($q2->result_array() as $row){
					$temp1 = explode(",",$row['survey']);
					$cmnt = '';				
					foreach($temp1 as $key=>$tr){						
						if(is_numeric($tr)){
							$total[$key] =  $total[$key] + $tr;	
						}else{
							$cmnt .= $tr;
						}					
					}				
					
					$cmnt .= $row['comment'];
					if(trim($cmnt)!=''){
						$comment[] = $cmnt;	
					}				
				}
	
				$overall = array_sum($total)/$numresponden/count($data['surveyques']);			
				$finalarray[] = array($rowq->name,$rowq->title,substr($overall,0,4));				
				
			}
		}

		if(!empty($finalarray)){
			array_unshift($finalarray,array('Nama','Mata Kuliah','Nilai Overall'));
			array_to_csv($finalarray,"Export_Hasil_Survey_Tutor_".$period.".csv");	
		}		
	}
	
	public function hasil_survey_detail($id_assignment){
		$this->load->model('class_model','cls');
		$q = $this->cls->get_survey_result($id_assignment);
		$class = $this->tutor_model->get_class_by_id($id_assignment);
		
		if($q){
			$data['surveyques'] = array(
				array('Bagian Pendahuluan','A'),
				array('Keterampilan menarik perhatian mahasiswa (ice breaking, kuis, games)',true),
				array('Keterampilan memotivasi mahasiswa',true),	
				array('Keterampilan menjelaskan ruang lingkup materi yang akan dibahas',true),
				array('Keterampilan menyampaikan relevansidanmanfaatmateri yang dibahas',true),
				array('Keterampilan menjelaskan kompetensi mahasiswa dalam mengikuti tutorial ',true),
				array('Bagian Penyajian ','B'),
				array('Sistematika penyampaian materi tutorial',true),
				array('Keterampilan menjelaskan materi tutorial',true),
				array('Keterampilan membimbing mahasiswa berlatih',true),
				array('Keterampilan mengajukan pertanyaan',true),
				array('Keterampilan memberikan umpan balik terhadap pertanyaan mahasiswa',true),
				array('Keterampilan menyebarkan pertanyaan kepada mahasiswa',true),
				array('Keterampilan mengaktifkan mahasiswa dalam tutorial',true),
				array('Keterampilan menggunakan media dan  alat pembelajaran',true),
				array('Keterampilan menggunakan beragam metode/model tutorial',true),
				array('Bagian Penutup','C'),
				array('Keterampilan menyimpulkan materi tutorial',true),
				array('Keterampilan mengevaluasi hasil belajar mahasiswa dalam tutorial',true),
				array('Keterampilan memberikan umpan balik terhadap hasil belajar mahasiswa dalam tutorial',true),
				array('Keterampilam memberikan informasi tentang kegiatan tindak lanjut',true),
				array('Keterampilan mengelola waktu tutorial ',true),
				array('Keseluruhan',true),
			);
			
			$total = array();
			//initializing
			foreach($data['surveyques'] as $key=>$p){
				$total[$key] = 0;
			}
			
			$comment = array();
			

			$numresponden = $q->num_rows();
			
			foreach($q->result_array() as $row){
				$temp1 = explode(",",$row['survey']);
				$cmnt = '';				
				foreach($temp1 as $key=>$tr){						
					if(is_numeric($tr)){
						$total[$key] =  $total[$key] + $tr;	
					}else{
						$cmnt .= $tr;
					}					
				}				
				
				$cmnt .= $row['comment'];
				if(trim($cmnt)!=''){
					$comment[] = $cmnt;	
				}				
			}

			$overall = array_sum($total)/$numresponden/count($data['surveyques']);
			
			
			$data['total'] = $total;
			$data['comment'] = $comment;
			$data['numresponden'] = $numresponden;
			$data['overall'] = $overall;
			$data['class'] = $class;
                        $data['id_assignment']=$id_assignment;
			
			$content['page'] = $this->load->view('tutor/hasil_survey_detail',$data,TRUE);
			$this->load->view('dashboard',$content);
		}
	}
	
	public function getlistJQGRIDcourse(){
		
		$this->load->model('tutor_model','tutor');

		$page = $this->input->post("page", TRUE );
		if(!$page)$page=1;
		
		$rows = $this->input->post("rows", TRUE );
		if(!$rows)$rows=10;
		
		$sort_by = $this->input->post( "sidx", TRUE );
		if(!$sort_by)$sort_by='title';
		
		$sort_direction = $this->input->post( "sord", TRUE );
		if(!$sort_direction)$sort_direction='ASC';
		
		$req_param = array (
            "sort_by" => $sort_by,
			"sort_direction" => $sort_direction,
			"page" => $page,
			"rows" => $rows,
			"search" => $this->input->post( "_search", TRUE ),
			"search_field" => $this->input->post( "searchField", TRUE ),
			"search_operator" => $this->input->post( "searchOper", TRUE ),
			"search_str" => $this->input->post( "searchString", TRUE )
		);
        
        $data = new stdClass();

		$data->page = $page;
        

		$data->records = count ($this->tutor->get_list_JQGRID_course($req_param,"all")->result_array());		
		$records = $this->tutor->get_list_JQGRID_course($req_param,"current")->result_array();

		$data->total = ceil($data->records /$rows );
		$data->rows = $records;

		echo json_encode ($data );
		exit( 0 );
	}


	public function export_survey_detail_to_excel($id_assignment) 
	{
		
		$this->load->model('class_model','cls');
		$q = $this->cls->get_survey_result($id_assignment);
		$class = $this->tutor_model->get_class_by_id($id_assignment);
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		
		$current_period = get_settings('time_period');
		
		$style_title = array(					
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'font' => array(
						'bold' => true,
					)
				);
				
		$style_table_header = array(					
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
						),
					)
				);
				
				
		$style_table = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
						),
					)
				);
				
		$this->excel->getDefaultStyle()->getFont()->setName('Verdana');
		$this->excel->getDefaultStyle()->getFont()->setSize(10);
		
		// Add an image to the worksheet
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('UT LOGO');		
		$objDrawing->setPath('assets/core/images/logo_ut.jpg');
		$objDrawing->setWidth(40);
		$objDrawing->setHeight(40);
		$objDrawing->setCoordinates('A1'); 
		$objDrawing->setWorksheet($this->excel->getActiveSheet()); 
		
		$this->excel->getActiveSheet()->setTitle('Survey Tutor');
		
			
		if($q){
			$data['surveyques'] = array( 
				array('Bagian Pendahuluan','A'), 
				array('Keterampilan menarik perhatian mahasiswa (ice breaking, kuis, games)',true), 
				array('Keterampilan memotivasi mahasiswa',true), 	
				array('Keterampilan menjelaskan ruang lingkup materi yang akan dibahas',true),
				array('Keterampilan menyampaikan relevansidanmanfaatmateri yang dibahas',true),
				array('Keterampilan menjelaskan kompetensi mahasiswa dalam mengikuti tutorial ',true), 
				array('Bagian Penyajian ','B'),
				array('Sistematika penyampaian materi tutorial',true),
				array('Keterampilan menjelaskan materi tutorial',true),
				array('Keterampilan membimbing mahasiswa berlatih',true),
				array('Keterampilan mengajukan pertanyaan',true),
				array('Keterampilan memberikan umpan balik terhadap pertanyaan mahasiswa',true),
				array('Keterampilan menyebarkan pertanyaan kepada mahasiswa',true),
				array('Keterampilan mengaktifkan mahasiswa dalam tutorial',true),
				array('Keterampilan menggunakan media dan  alat pembelajaran',true),
				array('Keterampilan menggunakan beragam metode/model tutorial',true), 
				array('Bagian Penutup','C'), 
				array('Keterampilan menyimpulkan materi tutorial',true),
				array('Keterampilan mengevaluasi hasil belajar mahasiswa dalam tutorial',true),
				array('Keterampilan memberikan umpan balik terhadap hasil belajar mahasiswa dalam tutorial',true),
				array('Keterampilam memberikan informasi tentang kegiatan tindak lanjut',true),
				array('Keterampilan mengelola waktu tutorial ',true), 
				array('Keseluruhan',true), 
			);
			
			$total = array(); 
			//initializing
			foreach($data['surveyques'] as $key=>$p){
				$total[$key] = 0; 
			} 
			
			$comment = array(); 
			

			$numresponden = $q->num_rows(); 
			
			foreach($q->result_array() as $row){ 
				$temp1 = explode(",",$row['survey']); 
				$cmnt = '';				 
				foreach($temp1 as $key=>$tr){						 
					if(is_numeric($tr)){ 
						$total[$key] =  $total[$key] + $tr;	 
					}else{ 
						$cmnt .= $tr;
					}					 
				}				
				
				$cmnt .= $row['comment']; 
				if(trim($cmnt)!=''){ 
					$comment[] = $cmnt; 	
				}				 
			} 

			$overall = array_sum($total)/$numresponden/count($data['surveyques']);
			
			
			/*$data['total'] = $total;
			$data['comment'] = $comment;
			$data['numresponden'] = $numresponden;
			$data['overall'] = $overall;
			$data['class'] = $class;*/
			

		}
		$this->excel->getActiveSheet()->setCellValue('A4', 'Tutor:'); 
		$this->excel->getActiveSheet()->setCellValue('B4', $class->name);
		$this->excel->getActiveSheet()->setCellValue('A5', 'Class Name:'); 
		$this->excel->getActiveSheet()->setCellValue('B5', $class->title);
		$this->excel->getActiveSheet()->setCellValue('A6', 'Over All:'); 
		$this->excel->getActiveSheet()->setCellValue('B6', substr($overall,0,4));
		
		$this->excel->getActiveSheet()->setCellValue('A8', 'No');
		$this->excel->getActiveSheet()->setCellValue('B8', 'Aspek yang diamati');
		$this->excel->getActiveSheet()->setCellValue('C8', 'Rata - rata Mutu*');
		//$this->excel->getActiveSheet()->getStyle('A8:C8')->applyFromArray($style_table_header);	
		
               // $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(129);
		
		$i = 1;$a = 1;$skip=0; $startrow=9;
			foreach($data['surveyques']as $key=>$row){ 
			if($row[1]!==true){
					
					$this->excel->getActiveSheet()->setCellValue('A'.$startrow, $row[1]);
                                        $this->excel->getActiveSheet()->getStyle('A'.$startrow.':C'.$startrow)->applyFromArray($style_table_header);
					$i = 0;
					$skip++;
				}else{					
					$this->excel->getActiveSheet()->setCellValue('A'.$startrow, $i);
				}
				if($i!=0){
					$this->excel->getActiveSheet()->setCellValue('B'.$startrow, $row[0]);
				}else{
				
					$this->excel->getActiveSheet()->setCellValue('B'.$startrow, $row[0]);
                                        $this->excel->getActiveSheet()->getStyle('A'.$startrow.':C'.$startrow)->applyFromArray($style_table_header);
				} 		
				 if($i!=0){ 
				
				$this->excel->getActiveSheet()->setCellValue('C'.$startrow, substr($total[$key-$skip]/$numresponden,0,4));
				 } 	
			$a++;
			$i++;	
			$startrow++;			
			 }
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(588);

                $this->excel->getActiveSheet()->setCellValue('A'.$startrow, 'Comment');
                $this->excel->getActiveSheet()->getStyle('A'.$startrow.':C'.$startrow)->applyFromArray($style_table_header);
                $x=1;
                foreach($comment as $row2){
                  $startrow++;
                  $this->excel->getActiveSheet()->setCellValue('A'.$startrow,$x);
                  $this->excel->getActiveSheet()->setCellValue('B'.$startrow,$row2);
                  $x++;
                }
		

		$filename='Survey Kelas _'.$class->name.'_'.$class->title.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		     
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');		
		$objWriter->save('php://output');
	}



public function absen_tutor() 
{		
		
		 $this->auth->check_auth();
		 
		 $this->load->library('form_validation');
		 $this->form_validation->set_rules('tanggalttm', 'Tanggal TTM', 'required');
		 $this->form_validation->set_rules('matkul', 'Mata Kuliah', 'required');
		 $this->form_validation->set_rules('pertemuan', 'TTM Ke-', 'required');
		 $this->form_validation->set_rules('status', 'Status TTM', 'required');
		 $data['error']="";
		 $data['success']=0;
		 
		 if($this->form_validation->run()){
		 	$data = array();
		 	$data = $this->input->post();
			$this->tutor_model->save_absen($data);
			 $data['error']="";
			$data['success'] = 1;
		 }else{
			  $data['success']=0;
			$data['error'] = validation_errors();
		}
		
		$distinct_assg=$this->tutor_model->get_distinct_assigmn($this->session->userdata('id'))->result_array();
		$gabungan=array(); 
		$list=array(); 
		 foreach($distinct_assg as $a){
                 $temp=$this->class_model->find_gabung_kelas($a['id']);
                 if(!empty($temp)){
                    if($temp[0]['from_assignment']==$a['id']){
					 $gabungan[$temp[0]['to_assignment']]=$a['id'];
					}else{
					 $gabungan[$a['id']]=$a['id'];
					}
                 }
                }
				foreach($distinct_assg as $a){if(empty($gabungan[$a['id']]) && (array_search($a['id'],$gabungan)=="")){ $gabungan[$a['id']]=$a['id'];}}
				$i=0;
				foreach($gabungan as $key => $val)
							{
								$detil = $this->class_model->detil_kelas($val);  
								//echo "<option value='".id_to_assigment($val)."'>(".$detil['code'].") ".$detil['title']." </option>";
								$list[$i]['id_assignment']	=id_to_assigment($val);
								$list[$i]['code']			=$detil['code'];
								$list[$i]['title']			=$detil['title'];

								$a=$this->tutor_model->get_absen_tutor(id_to_assigment($val));
								if($a!=false){
								$absen=$a->result_array();
								$list[$i]['absen']			=$absen[0]['date'];
								$list[$i]['status']			=$absen[0]['status'];
								}else{
								$list[$i]['absen']			="";
								$list[$i]['status']			="";
									
								}
								
								$i++;
							} 
		 $data['gabungan']=$list; 
		 $content['page'] = $this->load->view('tutor/absen_tutor',$data,TRUE);
		$this->load->view('dashboard',$content);
	
	}


public function list_absen(){
		$tutor=$this->tutor_model->get_all_distinct_assigmn()->result_array();
		$i=0;
		$list=array();  
		foreach($tutor as $key => $var){
		$distinct_assg=$this->tutor_model->get_distinct_assigmn($var['id'])->result_array();
		$gabungan=array(); 
		 foreach($distinct_assg as $a){
            $temp=$this->class_model->find_gabung_kelas($a['id']);
            if(!empty($temp)){
                if($temp[0]['from_assignment']==$a['id']){
					 $gabungan[$temp[0]['to_assignment']]=$a['id'];
				}else{
					 $gabungan[$a['id']]=$a['id'];
				}
            }
        }
		foreach($distinct_assg as $a){if(empty($gabungan[$a['id']]) && (array_search($a['id'],$gabungan)=="")){ $gabungan[$a['id']]=$a['id'];}}

			foreach($gabungan as $key => $val){
				$detil = $this->class_model->detil_kelas($val);  
				$staff = $this->person_model->get_staff_by_id($detil['staff_id']);  
				$a=$this->tutor_model->get_absen_tutor(id_to_assigment($val));
					if($a!=false){
						$absen=$a->result_array();
						$list[$i]['absen']	=$absen[0]['date'];
						$list[$i]['status']	=$absen[0]['status'];
					}else{
						$list[$i]['absen']	="";
						$list[$i]['status']	="";
					}
						$list[$i]['name']=$staff['name'];			
						$list[$i]['code']=$detil['code'];			
						$list[$i]['title']=	$detil['title'];		
					$i++;
				//echo $staff['name']."  #  (".$detil['code'].") ".$detil['title']."<br/>";
			}
		}
		$data['gabungan']=$list; 
		$content['page'] = $this->load->view('tutor/list_absen',$data,TRUE);
		$this->load->view('dashboard',$content);	
	}

	function cellColor($objPHPExcel,$cells,$color){
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array('rgb' => $color)
        ));
    }

public function export_absen_tutor() 
	{
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		
		$current_period = get_settings('time_period');
		
		$style_title = array(					
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'font' => array(
						'bold' => true,
					)
				);
				
		$style_table_header = array(					
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
						),
					)
				);
				
				
		$style_table = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
						),
					)
				);
				
		$this->excel->getDefaultStyle()->getFont()->setName('Verdana');
		$this->excel->getDefaultStyle()->getFont()->setSize(10);
		
		// Add an image to the worksheet
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('UT LOGO');		
		$objDrawing->setPath('assets/core/images/logo_ut.jpg');
		$objDrawing->setWidth(40);
		$objDrawing->setHeight(40);
		$objDrawing->setCoordinates('A1'); 
		$objDrawing->setWorksheet($this->excel->getActiveSheet()); 
		
		$this->excel->getActiveSheet()->setTitle('Absen Tutor Tahun Ajaran '.$current_period);
		$this->excel->getActiveSheet()->setCellValue('B2','Absen Tutor Tahun Ajaran '.$current_period);
		
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($style_title);
		$this->excel->getActiveSheet()->getStyle("B2")->getFont()->setBold(true)->setName('Verdana')->setSize(20);
		$this->excel->getActiveSheet()->getStyle('A4:J5')->applyFromArray($style_table);
		$this->excel->getActiveSheet()->mergeCells('B2:J2');
		
		$this->excel->getActiveSheet()->mergeCells('A4:A5');
		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:J4');
		$this->excel->getActiveSheet()->setCellValue('A4', 'Tutor'); 
		$this->excel->getActiveSheet()->setCellValue('B4', 'Mata Kuliah');
		$this->excel->getActiveSheet()->setCellValue('C4', 'TTM Ke-');
		$this->excel->getActiveSheet()->setCellValue('C5', '1');
		$this->excel->getActiveSheet()->setCellValue('D5', '2');
		$this->excel->getActiveSheet()->setCellValue('E5', '3');
		$this->excel->getActiveSheet()->setCellValue('F5', '4');
		$this->excel->getActiveSheet()->setCellValue('G5', '5');
		$this->excel->getActiveSheet()->setCellValue('H5', '6');
		$this->excel->getActiveSheet()->setCellValue('I5', '7');
		$this->excel->getActiveSheet()->setCellValue('J5', '8');
		$this->excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_title);
		$this->excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_title);
		$this->excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_title);
        
		
		$this->excel->getActiveSheet()->setCellValue('L4', 'Ketrangan :');
		$this->excel->getActiveSheet()->setCellValue('M4', 'online');
		$this->cellColor($this->excel,'M4', color_info_excel('online'));
		$this->excel->getActiveSheet()->setCellValue('M5', 'hadir ttm');
		$this->cellColor($this->excel,'M5', color_info_excel('hadir ttm'));
		$this->excel->getActiveSheet()->setCellValue('M6', 'pengganti');
		$this->cellColor($this->excel,'M6', color_info_excel('Pengganti'));
		
		$tutor=$this->tutor_model->get_all_distinct_assigmn()->result_array();
		$i=6;
		$list=array();  
		foreach($tutor as $key => $var){
		$distinct_assg=$this->tutor_model->get_distinct_assigmn($var['id'])->result_array();
		$gabungan=array(); 
		 foreach($distinct_assg as $a){
            $temp=$this->class_model->find_gabung_kelas($a['id']);
            if(!empty($temp)){
                if($temp[0]['from_assignment']==$a['id']){
					 $gabungan[$temp[0]['to_assignment']]=$a['id'];
				}else{
					 $gabungan[$a['id']]=$a['id'];
				}
            }
        }
		
		foreach($distinct_assg as $a){if(empty($gabungan[$a['id']]) && (array_search($a['id'],$gabungan)=="")){ $gabungan[$a['id']]=$a['id'];}}

			foreach($gabungan as $key => $val){
				$detil = $this->class_model->detil_kelas($val);  
				$staff = $this->person_model->get_staff_by_id($detil['staff_id']);  
				$a=$this->tutor_model->get_absen_tutor(id_to_assigment($val));
					if($a!=false){
						$absen=$a->result_array();
						$ab=explode("#",$absen[0]['date']);
						$st=explode("#",$absen[0]['status']);
							if($ab[0]!=0){
								if($st[0]=="hadir ttm" || $st[0]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('C'.$i, $ab[0]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('C'.$i, $ab[0]."\n".$st[0]);
								}
								$this->cellColor($this->excel,'C'.$i, color_info_excel($st[0]));
							}
							if($ab[1]!=0){
								if($st[1]=="hadir ttm" || $st[1]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('D'.$i, $ab[1]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('D'.$i, $ab[1]."\n".$st[1]);
								}
								$this->cellColor($this->excel,'D'.$i, color_info_excel($st[1]));
							}
							if($ab[2]!=0){
								if($st[2]=="hadir ttm" || $st[2]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('E'.$i, $ab[2]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('E'.$i, $ab[2]."\n".$st[2]);
								}
								$this->cellColor($this->excel,'E'.$i, color_info_excel($st[2]));
							}
							if($ab[3]!=0){
								if($st[3]=="hadir ttm" || $st[3]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('F'.$i, $ab[3]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('F'.$i, $ab[3]."\n".$st[3]);
								}
								$this->cellColor($this->excel,'F'.$i, color_info_excel($st[3]));
							}
							if($ab[4]!=0){
								if($st[4]=="hadir ttm" || $st[4]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('G'.$i, $ab[4]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('G'.$i, $ab[4]."\n".$st[4]);
								}
								$this->cellColor($this->excel,'G'.$i, color_info_excel($st[4]));
							}
							if($ab[5]!=0){
								if($st[5]=="hadir ttm" || $st[5]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('H'.$i, $ab[5]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('H'.$i, $ab[5]."\n".$st[5]);
								}
								$this->cellColor($this->excel,'H'.$i, color_info_excel($st[5]));
							}
							if($ab[6]!=0){
								if($st[6]=="hadir ttm" || $st[6]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('I'.$i, $ab[6]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('I'.$i, $ab[6]."\n".$st[6]);
								}
								$this->cellColor($this->excel,'I'.$i, color_info_excel($st[6]));
							}
							if($ab[7]!=0){
								if($st[7]=="hadir ttm" || $st[7]=="online" ){
									$this->excel->getActiveSheet()->setCellValue('J'.$i, $ab[7]);
								}else{
									$this->excel->getActiveSheet()->setCellValue('J'.$i, $ab[7]."\n".$st[7]);
								}
								$this->cellColor($this->excel,'J'.$i, color_info_excel($st[7]));
							}
						$this->excel->getActiveSheet()->getStyle('C'.$i.':J'.$i)->getAlignment()->setWrapText(true);
						
					}
						$this->excel->getActiveSheet()->setCellValue('A'.$i, $staff['name']); 
						$this->excel->getActiveSheet()->setCellValue('B'.$i, $detil['title']);
						$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(29);
						$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(36);
						$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
						$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
						
						if($i%2==0){
						$this->cellColor($this->excel,'A'.$i, 'D9D9D9');
						$this->cellColor($this->excel,'B'.$i, 'D9D9D9');
						}
						$this->excel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->applyFromArray($style_table);
					$i++;
				
			}
		}

		$filename='absen_tutor.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		     
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');		
		$objWriter->save('php://output');
	}
}


