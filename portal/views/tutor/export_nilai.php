<script>
function selectperiod(){
  var myselect = document.getElementById("selectperiod");
  window.location = "<?php echo base_url('tutor/export_nilai'); ?>/"+myselect.options[myselect.selectedIndex].value;
}
</script>
<table><tr>
<td><h1>Periode: </h1></td>
<td>
<select id="selectperiod" onchange="selectperiod()">
	<?php foreach($period_list as $per){?>
		<option value="<?php echo $per;?>" <?php echo $per==$curr_period?"SELECTED":"";?>><?php echo $per;?></option>
	<?php }?>
</select>
</td>
</tr></table>
<br/>

<?php if($list){?>
<table>
	<thead>
		<tr>
			<td>Nama</td><td>Course</td><td>Region</td><td></td>
		</tr>
	</thead>
<?php foreach($list->result() as $row){ ?>
	<tr>
		<td><?php echo $row->name; ?></td>
		<td><?php echo $row->title; ?></td>
		<td><?php echo (($row->sregion==1)?'Utara':'Selatan'); ?></td>
		<td><a href="<?php echo site_url('tutor/export_to_excel/'.$row->sid); ?>"><button>Download</button></a></td>		
	</tr>
<?php } ?>
</table>
<?php } ?>
