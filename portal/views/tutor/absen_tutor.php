<?php if($success){
	echo success_form("Terima kasih telah mengisi form absen");
}?>
<?php if($error != ''){
	echo error_form($error);
}
$absen=array();
?>
<div id="accordion">
	 <h3 style="padding:6px;font-size:12pt;padding-left:26px;">Form Absen</h3>
  	 <div>
		<?php echo form_open(current_url(), array('id'=>'frmTransport')); ?>
		<fieldset>
			<table>
				
				<tr>
					<td  width="150px"><label>Mata kuliah</label></td>
					<td>
						<select name="matkul">
							<?php
							$i=0;
							foreach($gabungan as $key => $val)
							{
								echo "<option value='".$val['id_assignment']."'>(".$val['code'].") ".$val['title']." </option>";
								
							} ?>
						</select>
					</td>                
				</tr>
				<tr>
					<td  width="150px"><label>TTM Ke</label></td>
					<td>
						<select name="pertemuan">
							<?php for($i=1;$i<=8;$i++){
								echo "<option value='".$i."'>".$i." </option>";
							} ?>
						</select>
					</td>                
				</tr>
				<tr>
					<td  width="150px"><label>Tanggal TTM</label></td>
					<td>
						<input type="text" name="tanggalttm" />
					</td>                
				</tr>
				<tr>
					<td  width="150px"><label>Status TTM</label></td>
					<td>
						<input type="radio" name="status" value="hadir ttm" checked/> Hadir TTM <br/>
						<input type="radio" name="status" value="online"/> Online <br/>
						<input type="radio" name="status" value="ganti"/> Digantikan tutor lain &nbsp; <input type="text" name="pengganti"/> (tulis nama tutor penggantinya)
					</td>                
				</tr>
				
				
				<tr>
					<td colspan="3"><button type="submit">
					<?php echo $this->lang->line('submit');?>
					</button></td>
				</tr>		
			</table>				
		</fieldset>	
		
		</form>
	</div>
</div>
<br />
<h3>Status Absen Mengajar</h3>
<table>
	<thead>
		<tr>
            <th rowspan="2">Mata Kuliah</th>
            <th colspan="8">Absen TTM</th>
        </tr>
        <tr>
            <th>TTM 1</th>
            <th>TTM 2</th>
            <th>TTM 3</th>
            <th>TTM 4</th>
            <th>TTM 5</th>
            <th>TTM 6</th>
            <th>TTM 7</th>
            <th>TTM 8</th>
        </tr>
	</thead>
	<?php

	   if(isset($gabungan)){
		
		foreach($gabungan as $key => $val){ 
		if($val['absen']!=""){
		$ab=explode("#",$val['absen']);
		$st=explode("#",$val['status']);
		?>
			<tr>
				<td><?php echo $val['title'];?></td>
				<td><?php if($ab[0]!=0){echo "<span class='button-link ".color_info($st[0])."' rel='tooltip' original-title='".$st[0]."'>".$ab[0]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[1]!=0){echo "<span class='button-link ".color_info($st[1])."' rel='tooltip' original-title='".$st[1]."'>".$ab[1]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[2]!=0){echo "<span class='button-link ".color_info($st[2])."' rel='tooltip' original-title='".$st[2]."'>".$ab[2]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[3]!=0){echo "<span class='button-link ".color_info($st[3])."' rel='tooltip' original-title='".$st[3]."'>".$ab[3]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[4]!=0){echo "<span class='button-link ".color_info($st[4])."' rel='tooltip' original-title='".$st[4]."'>".$ab[4]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[5]!=0){echo "<span class='button-link ".color_info($st[5])."' rel='tooltip' original-title='".$st[5]."'>".$ab[5]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[6]!=0){echo "<span class='button-link ".color_info($st[6])."' rel='tooltip' original-title='".$st[6]."'>".$ab[6]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				<td><?php if($ab[7]!=0){echo "<span class='button-link ".color_info($st[7])."' rel='tooltip' original-title='".$st[7]."'>".$ab[7]."<span>";}else{echo "<span class='button-link red'>Null<span>";}?></td>
				
			</tr>
		<?php }else{
			?>
<tr>
			<td><?php echo $val['title'];?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
				<td><?php echo "<span class='button-link red'>Null<span>";?></td>
</tr>
			<?php
			
		}
		
		}
	}else{ echo '<tr><td colspan="4"><center>Belum ada data absen pada semester ini</center></td></tr>';}?>
</table>
<script type="text/javascript" >
$(document).ready(function(){		
		$( "input[name=tanggalttm]" ).datepicker({
		changeMonth: true,
		changeYear: true,		
		dateFormat: "yy-mm-dd"

		});
		$( "#accordion" ).accordion({
	      collapsible: true,
	      active: false
	    });

});
</script>
