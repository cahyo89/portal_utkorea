<script src="<?php echo admin_tpl_path()?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="<?php echo admin_tpl_path()?>js/jqgrid/js/jquery.jqGrid.src.js" type="text/javascript"></script>
<script src="<?php echo admin_tpl_path()?>js/jquery.blockUI.js" type="text/javascript"></script>

  
<link rel="stylesheet" type="text/css" href="<?php echo admin_tpl_path()?>js/jqueryui/css/blitzer/jquery-ui-1.8.23.custom.css" />	
<link rel="stylesheet" type="text/css" href="<?php echo admin_tpl_path()?>css/add.css" />	
<link rel="stylesheet" type="text/css" href="<?php echo admin_tpl_path()?>js/jqgrid/css/ui.jqgrid.css" />	
 
  
  <script type="text/javascript">
	$(document).ready(function(){			
		$("#grid_name").jqGrid({
					url:'<?php
						  echo site_url( "tutor/getlistJQGRIDcourse" );
						  ?>',
					datatype: "json",
					colNames:['Kode', 'Mata Kuliah','SKS','Major','Semester','Tutorial','Key'],	
					colModel:[						
						{name:'code',width:100,index:'code',editable:false,editrules:{required:true}},
						{name:'title',width:400,index:'title',editable:false,editrules:{edithidden:true,requried:true}},
						{name:'credit',width:50,index:'credit',editable:true,editrules:{edithidden:true,requried:true}},
						{name:'major',width:400,index:'major',editable:true,editrules:{edithidden:true,requried:true},
							stype:'select',
							searchoptions:{value:{
							<?php 
								$first=1;foreach ($major as $key=>$val) {
									echo($first?"":",")."'".$key."':'".$val."'";
									$first=0;
								} 
							?> 
							}},
						
							edittype:'select',formatter:'select',
							editoptions:{value:{
							<?php 
								$first=1;foreach ($major as $key=>$val) {
									echo($first?"":",")."'".$key."':'".$val."'";
									$first=0;
								} 
							?> 
							}}
						},
				
						{name:'semester',width:50,index:'semester',editable:true,editrules:{edithidden:true,required:true,number:true},
							stype:'select',
							searchoptions:{value:{
							<?php 
								$first=1;for ($x=1; $x<=8; $x++) {
									echo($first?"":",").$x.":".$x;
									$first=0;
								} 
							?>
							}},
						
							edittype:'select',formatter:'select',
							editoptions:{value:{
							<?php 
								$first=1;for ($x=1; $x<=8; $x++) {
									echo($first?"":",").$x.":".$x;
									$first=0;
								} 
							?>
							}}
						},			
						{name:'tutorial',index:'tutorial',editable:true,stype:'select',searchoptions:{value:{'1':'Aktif','0':'Tidak Aktif'}}, edittype:'select',formatter:'select',editoptions:{value:{'1':'YES','0':'no'}}},	
						{name:'course_id',index:'course_id',editable:true,hidden:true}
					],
					mtype : "POST",		
					editurl: "<?php echo site_url( "superuser/" ); ?>",
					sortname: 'title',
					rownumbers: true,
					pager: "#pager2",
					autowidth: true,
					height: "100%",					
					viewrecords: true,					
					sortorder: "ASC",					
					jsonReader: { repeatitems : false, id: "8"}
				}).navGrid('#pager2',{edit:true,add:true,del:true, search: true},{
					resize:false,					
					afterComplete: function(data){						
						if(data.responseText!=''){alert(data.responseText)};
					}
				},
					{resize:false,					
					afterComplete: function(data){						
						if(data.responseText!=''){alert(data.responseText)};
					}
				},
					{resize:false,					
					afterComplete: function(data){						
						if(data.responseText!=''){alert(data.responseText)};
					}
				},{					
					sopt:['cn']
				}).navButtonAdd("#pager2",{caption:"Export Current Table",buttonicon:"ui-icon-bookmark",
					onClickButton:function(){
						$("#grid_name").jqGrid('excelExport',{url:"<?php
						  echo site_url( "tutor/exportCurrentCRUD" );
						  ?>"});
				}, position: "last", title:"Export Current Table",cursor: "pointer"});				
								
	});
  </script>
</head>
<style>
	.ui-widget{
		font-size:11pt;
	}
</style>
<body>		
	<?php if (isset($content)) echo "<h3>".$content."</h3>";  else $content= '';?>			
    <table id="grid_name"></table>
    <div id="pager2" ></div>         