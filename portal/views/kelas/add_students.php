<script type="text/javascript">
	$(document).ready(function(){	
		$.get( '<?php echo site_url("kelas/add_students_list")?>/'+$("#hid"+$("#course").val()).val(), function( data ) {
			  $( "#result" ).html( data );
		});
		$('#id_assignment').val($("#course").val());
		$( "#course" ).change(function() {
			//alert($("#hid"+$(this).val()).val());
			$.get( '<?php echo site_url("kelas/add_students_list")?>/'+$("#hid"+$(this).val()).val(), function( data ) {
			  $( "#result" ).html( data );
			  
			});
			$('#id_assignment').val($("#course").val());
		});
	});
</script>

<div style="color:red">
	Menu ini hanya boleh digunakan untuk menambahkan mahasiswa yang sudah tidak bisa mendaftar kelas.<br/>
	(karena sebelumnya sudah melakukan pendaftaran).<br/>
	Beritahukan dahulu kepada mahasiswa untuk melakukan proses pendaftaran.<br/>
	Jika mahasiswa sudah tidak dapat mendaftar, tutor boleh menambahkan mahasiswa yang bersangkutan melalui menu ini.<br/>
</div>
<br/>

<?php 

if($classes){
	//print_r($classes->result());
	$hiddenData='';
	foreach($classes->result() as $row){
		$options[$row->id]=$row->title;	
		$hiddenData.="<input type='hidden' id='hid".$row->id."' value='".$row->major_id."/".$row->semester."/".$row->region_id."/".$row->course_id."/".$row->id."/".$row->staff_id."'>";		
	}
	$js = 'id="course"';
	echo form_dropdown('course', $options,null,$js);
	echo $hiddenData;
}else{
	echo "Belum ada kelas.";
}

?>

<form name="addstudentform" method="POST">
	<div id="result"></div>
	<br/>
	<input type="hidden" name="id_assignment" id="id_assignment" value=""/>
	<button type="submit">Tambahkan di kelas</button>
</form>