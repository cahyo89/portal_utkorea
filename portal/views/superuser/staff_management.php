<script src="<?php echo admin_tpl_path()?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="<?php echo admin_tpl_path()?>js/jqgrid/js/jquery.jqGrid.src.js" type="text/javascript"></script>
<script src="<?php echo admin_tpl_path()?>js/jquery.blockUI.js" type="text/javascript"></script>

  
<link rel="stylesheet" type="text/css" href="<?php echo admin_tpl_path()?>js/jqueryui/css/blitzer/jquery-ui-1.8.23.custom.css" />	
<link rel="stylesheet" type="text/css" href="<?php echo admin_tpl_path()?>css/add.css" />	
<link rel="stylesheet" type="text/css" href="<?php echo admin_tpl_path()?>js/jqgrid/css/ui.jqgrid.css" />	

  
  <script type="text/javascript">
	$(document).ready(function(){			
		$("#grid_name").jqGrid({
					url:'<?php
						  echo site_url( "humas/getlistJQGRID/staff" );
						  ?>',
					datatype: "json",
					colNames:['Nama', 'Username','Role','Aktif','Phone','Email','Key'],	
					colModel:[						
						{name:'name',width:500,index:'name',editable:true,editrules:{required:true}},
						{name:'username',width:200,index:'username',editable:true,editrules:{edithidden:true,requried:true}},
						{name:'group_id',width:300,index:'group_id',editable:true,editrules:{edithidden:true,required:true},size:2,edittype:'select',formatter:'select',
							stype:'select',
							searchoptions:{value:{
								<?php $first=1;foreach($usergroups as $role){
									echo ($first?"":",")."'".$role['usergroup_id']."':'".$role['group']."'";
									$first=0;
								}?>
							}}, 
							editoptions:{multiple:true,value:{
							<?php $first=1;foreach($usergroups as $role){
								echo ($first?"":",")."'".$role['usergroup_id']."':'".$role['group']."'";
								$first=0;
							}?>
							}}
						},		
						{name:'is_active',index:'is_active',editable:true,stype:'select',searchoptions:{value:{'1':'Aktif','0':'Tidak Aktif'}}, edittype:'select',formatter:'select',editoptions:{value:{'1':'YES','0':'no'}}},						
						{name:'phone',index:'phone',editable:true,editrules:{edithidden:true,required:true,number:true},hidden:true},									
						{name:'email',width:300,index:'email',editable:true,editrules:{edithidden:true,requried:true,email:true},hidden:true},
						{name:'staff_id',index:'staff_id',editable:true,hidden:true}
					],
					mtype : "POST",		
					editurl: "<?php echo site_url( "superuser/staffCRUD" ); ?>",
					sortname: 'name',
					rownumbers: true,
					pager: "#pager2",
					autowidth: true,
					height: "100%",					
					viewrecords: true,					
					sortorder: "ASC",					
					jsonReader: { repeatitems : false, id: "8"}
				}).navGrid('#pager2',{edit:true,add:true,del:true, search: true},{
					resize:false,					
					afterComplete: function(data){						
						if(data.responseText!=''){alert(data.responseText)};
					}
				},
					{resize:false,					
					afterComplete: function(data){						
						if(data.responseText!=''){alert(data.responseText)};
					}
				},
					{resize:false,					
					afterComplete: function(data){						
						if(data.responseText!=''){alert(data.responseText)};
					}
				},{					
					sopt:['cn']
				}).navButtonAdd("#pager2",{caption:"Export Current Table",buttonicon:"ui-icon-bookmark",
					onClickButton:function(){
						$("#grid_name").jqGrid('excelExport',{url:"<?php
						  echo site_url( "tutor/exportCurrentCRUD" );
						  ?>"});
				}, position: "last", title:"Export Current Table",cursor: "pointer"});				
								
	});
  </script>
</head>
<style>
	.ui-widget{
		font-size:11pt;
	}
</style>
<body>		
	<?php if (isset($content)) echo "<h3>".$content."</h3>";  else $content= '';?>			
    <table id="grid_name"></table>
    <div id="pager2" ></div>         