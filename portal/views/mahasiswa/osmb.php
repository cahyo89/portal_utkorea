<script type="text/javascript" >
	$(document).ready(function(){	
			
			if($("#osmb_join" ).val()==0){
					$( "#tr_lokasi" ).hide();
					$( "#tr_bus" ).hide();
			}else{
					$( "#tr_lokasi" ).show();
					$( "#tr_bus" ).show();
			}
				
			if($("#osmb_bus" ).val()==0){
					$( "#tr_lokasi" ).hide();
			}else{
					$( "#tr_lokasi" ).show();
			}	
				
			$( "#osmb_bus" ).change(function (){
				if($("#osmb_bus" ).val()==0){
					$( "#tr_lokasi" ).hide();
				}else{
					$( "#tr_lokasi" ).show();
				}
			});
			
			$( "#osmb_join" ).change(function (){
				if($("#osmb_join" ).val()==0){
					$( "#tr_lokasi" ).hide();
					$( "#tr_bus" ).hide();
				}else{
					$( "#tr_lokasi" ).show();
					$( "#tr_bus" ).show();
				}
			});
	});
</script>

<div style="color:red">
<?php if ( !empty($_POST) ) {?>
Data diperbarui.
<?}?>
</div>

<?php echo form_open_multipart(current_url(), array('id'=>'frmOSMB')); ?>

	<?php $meet_point=array(
					'0'=>'Seoul',
					'1'=>'Ansan',
					'2'=>'Daegu',
					'3'=>'Busan'
				);
				
	?>
	<fieldset>
		<table>
			<tr>
				<td width="150px"><label>Nama</label></td>
				<td><?php echo form_input(array('name'=>'osmb_name','size' => '50','value'=>$mahasiswa['name'],'readonly'=>'true'));?></td>
			</tr>
			
			<tr>
				<td><label>Telefon</label></td>
				<td><?php echo form_input(array('name'=>'osmb_telp','size' => '50','value'=>$_POST?$this->input->post('osmb_telp'):isset($osmb)?$osmb->phone:'0'.substr($mahasiswa['phone'],2)));?></td>
			</tr>
			
			
			<tr>
				<td><label>Kehadiran</label></td>
				<td><?php echo form_dropdown('osmb_join',array('0'=>'Tidak Hadir','1'=>'Hadir'),$_POST?$this->input->post('osmb_join'):isset($osmb)?$osmb->hadir:'1','id="osmb_join"');?></td>
			</tr>
			
			<tr id='tr_bus'>
				<td><label>Transportasi</label></td>
				<td><?php echo form_dropdown('osmb_bus',array('0'=>'Transportasi Sendiri','1'=>'Bus Bersama'),$_POST?$this->input->post('osmb_bus'):isset($osmb)?$osmb->bus:'1','id="osmb_bus"');?></td>
			</tr>
			
			<tr id='tr_lokasi'>
				<td><label>Lokasi Penjemputan</label></td>
				<td><?php echo form_dropdown('osmb_lokasi',$meet_point,isset($osmb)?$osmb->lokasi:$this->input->post('osmb_lokasi'),'id="osmb_lokasi"');?></td>
			</tr>
			
			<tr>
				<td colspan="3"><button type="submit">
				<?php echo $this->lang->line('submit');?>
				</button></td>
			</tr>
		</table>
	</fieldset>
</form>	