<?php
class announcement_model extends CI_Model {
    

    function __construct()
    {        
        parent::__construct();
    }
	
	public function get_recent_news() {
		$this->db->order_by('created_time','desc');
		$this->db->where('publish','1');
		$query = $this->db->get('announcements',10,0);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	
	public function save_announcement($data) {
		$insert = populate_form($data, 'announcements');
		$this->db->set('created_time', 'now()', FALSE);
		
		$query = $this->db->insert('announcements',$insert);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public function display($id) {
		$this->db->where('id',$id);
		$this->db->where('publish','1');
		$query = $this->db->get('announcements');
		
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
	
	public function get_all_announcements(){
		$query = $this->db->get('announcements');
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	
	public function update_announcement($data) {
		$update = populate_form($data, 'announcements');
		//$this->db->set('created_time', 'now()', FALSE);
		$this->db->where('id',$update['id']);
		return $this->db->update('announcements',$update);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public function get_announcement($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('announcements');
		
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
}